<?php

include_once __DIR__ . '/vendor/autoload.php';
session_start();

define('ROOT_PATH', __DIR__);
define('REPLACEMENTS_PATH', ROOT_PATH . '/replacement.json');

class Google_Drive_Direct_Link_Api
{

    private $url;
    private $app_name = 'NAMA APLIKASI';
    private $api_key = 'AIzaSyAwADTlGnFWpdJDZIeCX59S42CxIDkKsiI'; // api_key
    private $result = array(
        'error' => false
    );

    public function __construct($url) {
        $this->url = $url;
    }

    public function generate_link()
    {
        $client = $this->getClient();
        $file_id = $this->extract_file_id();
        $token_key = $this->getAccessToken($client);

        if( !empty( $file_id ) ) {

            $data = $this->request_data($file_id, $token_key);

            if( $data['error'] ) return $data['message'];

            $response = json_decode(str_replace(")]}'", '', $data['data']));
            // $response->scanResult = 'ERROR';
            if( $response->scanResult == 'ERROR' ) {

                $error = true;
                $replacements = $this->get_replacements();

                if( isset( $replacements[$file_id] ) ) {

                    $data = $this->request_data($replacements[$file_id], $token_key);
                    if( $data['error'] ) return $data['message'];
                    $response = json_decode(str_replace(")]}'", '', $data['data']));

                    if( $response->scanResult != 'ERROR' ) {
                        $error = false;
                    }

                }

                if( $error ) {
                    $copy_data = $this->request_copy_file($file_id, $client, $token_key);
                    if( $copy_data['error'] ) return $copy_data['message'];
                    $file_id = $copy_data['id'];
                    
                    $data = $this->request_data($file_id, $token_key);
                    
                    if( $data['error'] ) return $data['message'];
                    $response = json_decode(str_replace(")]}'", '', $data['data']));
                    
                    if( $response->scanResult == 'ERROR' ) {
                        return $response->disposition;
                    }
                }

            }
            if( !empty($response->downloadUrl) ) {

                $link = str_replace( 'e=download', 'e=preview', $response->downloadUrl );
                $link = rtrim($link, '&gd=true');
                $link = rtrim($link, '&authuser=0');
                return $link;

            }
        }
    }

    private function request_data($file_id, $token_key) 
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://drive.google.com/uc?id=$file_id&export=preview",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => array(
                "Accept: */*",
                "Accept-Encoding: gzip, deflate, br",
                "Accept-Language: en-US,en;q=0.5",
                "Connection: keep-alive",
                "Content-Length: 0",
                // "Authorization: Bearer $token_key",
                "Content-Type: application/x-www-form-urlencoded;charset=utf-8",
                "DNT: 1",
                "Host: drive.google.com",
                "Origin: https://drive.google.com",
                "Referer: https://drive.google.com/drive/u/0/my-drive",
                "TE: Trailers",
                "X-Drive-First-Party: DriveWebUi",
                "X-Json-Requested: true",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            $this->result['error'] = true;
            $this->result['message'] = "Error #:" . $err;
        } else {
            $this->result['data'] = $response;
        }
        return $this->result;

    }

    private function request_copy_file($file_id, $client, $token_key)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://www.googleapis.com/drive/v2/files/$file_id/copy?key={$this->api_key}",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Accept-Encoding: gzip, deflate",
            "Authorization: Bearer $token_key",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Content-Length: 0",
            "Content-Type: application/json",
            "Host: www.googleapis.com",
            "cache-control: no-cache"
          ),
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        
        if ($err) {
            $this->result['error'] = true;
            $this->result['message'] = "Error #:" . $err;
        } else {
            
            $data = json_decode($response, true);
            $service = new Google_Service_Drive($client);
            $userPermission = new Google_Service_Drive_Permission(array(
              'type' => 'anyone',
              'role' => 'reader'
            ));
            $service->permissions->create(
                $data['id'], $userPermission, array('fields' => 'id')
            );
            $this->update_replacement($file_id, $data['id']);
            $this->result['data'] = $data;
        }
        return $this->result;
    }

    private function extract_file_id()
    {
        $url = preg_replace( '#^https?://#', '', $this->url );
        $file_id = '';
        $array = explode('/', $url);
        for( $i = 0; $i < count($array); $i++ ) {
            if( in_array( trim($array[$i]), ['d', 'file', 'drive.google.com'] ) )
                unset($array[$i]);
        }
        $array = array_merge($array, []);
        return ( isset( $array[0] ) ) ? $array[0] : '';
    }

    private function get_replacements() 
    {
        $replacementPath = REPLACEMENTS_PATH;
        if( !file_exists($replacementPath) ) {
            file_put_contents($replacementPath, "{}");
        };
        $replacements = json_decode(file_get_contents($replacementPath), true);
        return $replacements;
    }

    private function update_replacement($old_id, $new_id)
    {
        $replacements = $this->get_replacements();
        $replacements[$old_id] = $new_id;
        file_put_contents(REPLACEMENTS_PATH, json_encode($replacements));
    }

    protected function getClient() 
    {
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . ROOT_PATH . '/credentials.json');
        $client = new Google_Client();
        $client->useApplicationDefaultCredentials();
        $client->setApplicationName($this->app_name);
        if( !empty($this->api_key) )
            $client->setDeveloperKey($this->api_key);
        $client->setScopes(Google_Service_Drive::DRIVE);
        return $client;
    }

    private function getAccessToken($client)
    {
        $accessToken = $client->fetchAccessTokenWithAssertion();
        return $accessToken['access_token'];
    }

}

$url = isset( $_GET['url'] ) ? $_GET['url'] : '';
$api = new Google_Drive_Direct_Link_Api( $url );
$link = $api->generate_link();
// echo "<pre>";
// var_dump($link);
// echo "<pre>";
// exit;
echo $link;